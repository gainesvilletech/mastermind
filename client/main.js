import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import tether from 'tether';
import Popper from 'popper.js';
window.Popper = Popper;
global.Tether = tether;
jQuery = $ = require('jquery');
bootstrap = require('bootstrap');

import App from '../imports/ui/App';

import './main.scss';

Meteor.startup(() => {
	render(<App />, document.getElementById('app'));
});
