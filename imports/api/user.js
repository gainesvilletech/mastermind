import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Accounts.onCreateUser((options, user) => {
  if (! user.services.twitter) {
    throw new Error('Expected login with Twitter only.');
  }
  const { profile_image_url, profile_image_url_https, screenName} = user.services.twitter;
  return Object.assign({}, user, {
    profile_image_url,
    profile_image_url_https,
    screenName,
  })
});

Meteor.publish('userData', function () {
  return Meteor.users.find({ _id: this.userId },
    { fields: {
      'profile_image_url': 1,
      'profile_image_url_https': 1,
      'screenName': 1,
    }});
});


