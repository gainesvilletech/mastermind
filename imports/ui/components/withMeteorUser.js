import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

export default (component) => createContainer(() => {
  const handle = Meteor.subscribe('userData');
  return {
    currentUser: Meteor.user(),
    loading: handle.ready(),
  }
}, component);

