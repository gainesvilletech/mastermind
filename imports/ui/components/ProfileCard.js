import React from 'react';
import { map } from 'lodash';
import { Card, CardImg, CardText, CardBlock,
  CardTitle, CardSubtitle, Button, Modal,
  ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { compose, withState } from 'recompose';

import Preference from '../components/Preference';

const ProfileCard = ({ index, categoryName, subcategories, isOpen, setIsOpen }) => (
  <div className="col-12 col-md-4">
    <Card>
      <CardImg top width="100%" src={`/${categoryName}.png`} alt="Card image cap" />
      <CardBlock>
        <CardTitle>{categoryName}</CardTitle>
        <CardSubtitle>Card subtitle</CardSubtitle>
        <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
        <Button onClick={() => setIsOpen(true)}>Choose Options</Button>

        <Modal isOpen={isOpen} toggle={() => setIsOpen(!isOpen)}>
          <ModalHeader toggle={() => setIsOpen(!isOpen)}>{categoryName}</ModalHeader>
          <ModalBody>
            { map(subcategories, (c, index) => <Preference key={index} categoryName={c} />) }
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => setIsOpen(!isOpen)}>Save</Button>{' '}
            <Button color="secondary" onClick={() => setIsOpen(!isOpen)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </CardBlock>
    </Card>
  </div>
);

const enhance = compose(
  withState('isOpen', 'setIsOpen', false)
);

export default enhance(ProfileCard);
