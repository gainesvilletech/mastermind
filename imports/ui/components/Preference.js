import React from 'react';
import { compose, withState } from 'recompose';

const Preference = ({ index, categoryName }) => (
  <div className="form-check">
    <label className="form-check-label">
      <input className="form-check-input" type="checkbox" name={index} id={index} value={index} />
      {categoryName}
    </label>
  </div>
);

const enhance = compose(

);

export default enhance(Preference);
