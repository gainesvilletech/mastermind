import React from 'react';
import { Badge, Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';
import { compose, withHandlers, withState } from 'recompose';
import { Meteor } from 'meteor/meteor';

const BarProfile = ({ user: { profile_image_url_https }, handleLogOut, dropdownOpen, handleToggle }) => (
  <div>
    <div style={{ textAlign: 'center' }}>
      <Dropdown isOpen={dropdownOpen} toggle={handleToggle}>
        <span
          onClick={handleToggle}
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded={dropdownOpen}
        >
          <img src={profile_image_url_https} className="rounded-circle cursor-pointer" />
        </span>
        <DropdownMenu right style={{ left: 'auto !important'}}>
          <DropdownItem onClick={handleLogOut}>Log out</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </div>
  </div>
);

const enhance = compose(
  withState('dropdownOpen', 'setDropdownOpen', false),
  withHandlers({
    handleToggle: ({ dropdownOpen, setDropdownOpen }) => () =>setDropdownOpen(!dropdownOpen),
    handleLogOut: ({ handleToggle }) => () => { Meteor.logout(); handleToggle(); },
  })
);

export default enhance(BarProfile);
