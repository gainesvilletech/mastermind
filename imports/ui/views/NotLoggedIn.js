import React from 'react';
import LoginWithTwitter from '../LoginWithTwitter';

const NotLoggedIn = () => (
  <div className="jumbotron">
    <h1>Welcome to Mastermind!</h1>
    <p><strong>Mastermind is matchmaking utility that pairs mentors and mentees in the Gainesville Community.</strong></p>
    <p>In order to get started, log in with twitter and create a profile. You can indicate if you'd like be a mentor or get mentored along with the attributes you're interested in. Then you can start browsing other community members!</p>
    <div className="text-center">
      <LoginWithTwitter />
    </div>
  </div>
);

export default NotLoggedIn;
