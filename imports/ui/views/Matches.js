import React from 'react';
import { map } from 'lodash';
import { compose, withProps, withState } from 'recompose';
import { Card, CardImg, CardTitle, CardDeck, FormGroup, Label, Input,
  CardSubtitle, CardFooter, CardBlock, Progress,
  Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const STUB_DATA = [
  {
    id: 1,
    image: 'https://media.licdn.com/media/AAEAAQAAAAAAAAy7AAAAJGFmNWRiYTc5LWFkNmEtNDU5NS1iNWJkLTNjNjNjODY2ZWIzNQ.jpg',
    name: 'Andrew Crites',
    bio: 'Principal Developer at Mobiquity',
    skills: [
      { title: 'AWS', percentage: 97 },
      { title: 'JavaScript', percentage: 65 },
      { title: 'Webapps', percentage: 87 },
      { title: 'General Programming', percentage: 65 },
    ],
    publications: [
      { title: 'Medium', icon: '/logos/Monogram.png', amount: 5}
    ]
  }, {
    id: 2,
    image: 'https://pbs.twimg.com/profile_images/883135154109206528/-2ykPBYR_400x400.jpg',
    name: 'Jon Church',
    bio: 'Chatbot Developer / More decisive every moment, hopefully',
    skills: [
      { title: 'Bots', percentage: 97 },
      { title: 'Webapps', percentage: 87 },
      { title: 'JavaScript', percentage: 80 },
      { title: 'IOT', percentage: 65 },
    ]
  }, {
    id: 3,
    image: 'https://pbs.twimg.com/profile_images/783671772298096644/1VqflNmw_400x400.jpg',
    name: 'Mike Herchel',
    bio: 'I like stupid things and my name is Mike! ',
    skills: [
      { title: 'Sucking', percentage: 89 },
      { title: 'Being an Asshat', percentage: 100 },
      { title: 'Being Attractive', percentage: 2 },
      { title: 'Shouting things out', percentage: 100 },
    ]
  }, {
    id: 4,
    image: 'https://pbs.twimg.com/profile_images/879027033292124160/8Mvhm5PM_400x400.jpg',
    name: 'Harry Gogonis',
    bio: 'Software Engineer / Javascript / React / Redux',
    skills: [
      { title: 'Crowdfunding', percentage: 89 },
      { title: 'User Experience', percentage: 100 },
      { title: 'Product Management', percentage: 25 },
      { title: 'Product & Design', percentage: 100 },
    ]
  }, {
    id: 5,
    image: 'https://ca.slack-edge.com/T024FJVCU-U029PNQH8-2f8fd5a3b03b-1024',
    name: 'Blake McLeod',
    bio: 'Frontend developer, with marketing background for hire!',
    skills: [
      { title: 'Kickstarter', percentage: 89 },
      { title: 'Growth', percentage: 100 },
      { title: 'Webapps', percentage: 75 },
      { title: 'JS', percentage: 100 },
    ]
  }, {
    id: 6,
    image: 'https://pbs.twimg.com/profile_images/677880317869350912/s3B3PM0I.jpg',
    name: 'Donald Trump',
    bio: 'Sleezebag shithead',
    skills: [
      { title: 'Tweeting meaningful thoughts', percentage: 0 },
      { title: 'Disrupting Order', percentage: 99 },
      { title: 'Hair Quality', percentage: 2 },
      { title: 'Destroying Nation', percentage: 99 },
    ]
  },
];

const Matches = ({ matches, matchInformation, setMatchInformation }) => (
  <div className="container matches">
    <h1>Recommended For You</h1>
    <CardDeck>
      { map(matches, match => (
        <Card key={match.id} style={{ flexBasis: '28%', marginBottom: '25px' }}>
          <CardImg top width="100%" src={match.image} alt={`${match.name} Profile Picture`} />
          <CardBlock>
            <CardTitle>{match.name}</CardTitle>
            <CardSubtitle>{match.bio}</CardSubtitle>
          </CardBlock>
          { match.skills && match.skills.length && (
            <ul className="list-group list-group-flush">
              { map(match.skills, (s, index) => (
                <li key={index} className="list-group-item">
                  <p>{s.title}</p>
                  <Progress value={s.percentage} />
                </li>
              ))}
            </ul>
          ) }
          <div>
            {match.publications &&
              <div>
                <img src={match.publications[0].icon} width={30} height={30} />
                <div style={{ display: 'inline-block', verticalAlign: 'middle', padding: '8px'}}>
                  Articles: {match.publications[0].amount}
                </div>
              </div>
            }
          </div>
          <CardFooter>
            <Button className="btn-outline-primary" type="button" onClick={() => setMatchInformation(match)} block>Contact</Button>
          </CardFooter>
        </Card>
      ))}
    </CardDeck>
    <Modal isOpen={!!matchInformation} toggle={() => setMatchInformation(null)}>
      <ModalHeader toggle={() => setMatchInformation(null)}>Send Message</ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label for="exampleText">Enter a Brief Message Here</Label>
          <Input type="textarea" name="text" id="exampleText" />
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button type="button" color="primary" onClick={() => setMatchInformation(null)}>Send</Button>{' '}
        <Button type="button" color="secondary" onClick={() => setMatchInformation(null)}>Cancel</Button>
      </ModalFooter>
    </Modal>
  </div>
);

const enhance = compose(
  withState('matchInformation', 'setMatchInformation', false),
  withProps({
    matches: STUB_DATA,
  }),
);

export default enhance(Matches);
