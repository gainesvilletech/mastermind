import React from 'react';
import { compose } from 'recompose';
import { map } from 'lodash';
import { CardDeck } from 'reactstrap';

import withMeteorUser from '../components/withMeteorUser';
import ProfileCard from '../components/ProfileCard';

const PROFILE_OPTIONS = [
  { categoryName: 'Tech', subcategories: [
    'iOS',
    'Android',
    'JS',
    'Webapps',
    'IOT',
    'Hardware',
    'General Programming',
    'WordPress',
    'Ruby',
    'CRMs',
    'Innovation',
    'Cloud',
  ]},
  { categoryName: 'Business', subcategories: [
    'Startup',
    'Growth',
    'Career Advice',
    'Branding',
    'Financial Consulting',
    'Customer Engagement',
    'Strategy',
    'Sectors',
    'Getting Started',
    'Human Resources',
    'Business Development',
    'Legal',
  ]},
  { categoryName: 'Funding', subcategories: [
    'Crowdfunding',
    'Kickstarter',
    'Venture Capital',
    'Finance',
    'Bootstrapping',
    'Nonprofit',
  ]},
  { categoryName: 'Product & Design', subcategories: [
    'Identity',
    'User Experience',
    'Lean Startup',
    'Product Management',
    'Metrics & Analytics',
  ]},
  { categoryName: 'Sales & Marketing', subcategories: [
    'Social Media Marketing',
    'Search Engine Optimization',
    'Public Relations',
    'Branding',
    'Publishing',
    'Inbound Marketing',
    'Email Marketing',
    'Copywriting',
    'Growth Strategy',
    'Search Engine Marketing',
    'Sales & Lead Generation',
    'Advertising',
    'Public Speaking',
    'Fostering Culture',
  ]},
  { categoryName: 'Industries', subcategories: [
    'SaaS',
    'E-commerce',
    'Education',
    'Real Estate',
    'Restaurant',
    'Retail',
    'Marketplaces',
    'Nonprofit',
  ]},
];

const Profile = () => (
  <div className="profile">
    <header>
      Profile
    </header>
    <div className="container">
      <h1>Select your preferences:</h1>
      <div className="row">
        { map(PROFILE_OPTIONS, (c, index) => <ProfileCard key={index} {...c} />) }
      </div>
    </div>
  </div>
);

const enhance = compose(
  withMeteorUser,
);

export default enhance(Profile);
