import React from 'react';
import { compose, withState, withHandlers, branch, renderComponent } from 'recompose';
import cn from 'classnames';

import withMeteorUser from './components/withMeteorUser';
import LoginWithTwitter from './LoginWithTwitter';
import NotLoggedIn from './views/NotLoggedIn';
import Profile from './views/Profile';
import Matches from './views/Matches';
import BarProfile from './views/BarProfile';
import NearMe from './views/NearMe';

const App = ({ currentView, navigateTo, loading, currentUser }) => {

  let content;
  switch (currentView) {
    case 'matches' : {
      content = <Matches />;
      break;
    }
    case 'profile' : {
      content = <Profile />;
      break;
    }
    case 'nearme' : {
      content = <NearMe />;
      break;
    }
    default : {
      content = <NotLoggedIn />;
      break;
    }
  }

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <a className="navbar-brand" href="#">MasterMind</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          { !!currentUser && (
            <ul className="navbar-nav mr-auto">
              <li className={cn('nav-item', { 'active': currentView === 'profile' })}>
                <a className="nav-link" href="#" onClick={navigateTo('profile')}>My Profile <span className="sr-only">(current)</span></a>
              </li>
              <li className={cn('nav-item', { 'active': currentView === 'matches' })}>
                <a className="nav-link" href="#" onClick={navigateTo('matches')}>Matches</a>
              </li>
              <li className={cn('nav-item', { 'active': currentView === 'nearme' })}>
                <a className="nav-link" href="#" onClick={navigateTo('nearme')}>Near Me</a>
              </li>
            </ul>
          )}
          <div className="ml-auto">
            {loading && currentUser && <BarProfile user={currentUser} />}
            {!currentUser && <LoginWithTwitter /> }
          </div>
        </div>
      </nav>
      <div className="" style={{ marginBottom: '25px' }}>{content}</div>
    </div>
  );
};

const enhance = compose(
  withMeteorUser,
  branch(({ loading }) => !loading, renderComponent(() => <p>Loading...</p>)),
  withState('currentView', 'setView', ({ currentUser }) => !!currentUser ? 'profile' : null),
  withHandlers({
    navigateTo: ({ setView }) => view => () => setView(view),
  }),
);

export default enhance(App);
