import React from 'react';
import { Meteor } from 'meteor/meteor';
import { compose, withHandlers } from 'recompose';
import { Button } from 'reactstrap';

const LoginWithTwitter = ({ handleClick }) => (
	<div>
		<Button onClick={handleClick} className="btn btn-info">Log in with Twitter</Button>
	</div>
);

const enhance = compose(
	withHandlers({
		handleClick: () => () => {
			Meteor.loginWithTwitter();
		}
	})
);

export default enhance(LoginWithTwitter);
