import { Meteor } from 'meteor/meteor';

const { consumerKey, secret } = Meteor.settings.twitter;

ServiceConfiguration.configurations.upsert(
  { service: 'twitter' },
  {
    $set: {
      loginStyle: "popup",
      consumerKey,
      secret,
    }
  }
);
